**tezNames** system consists of three contracts. _cTezname_ is the most important contract that contains all the necessary data fields and functions for the tezName. The other two contracts, _cGate_ and _cNameType_, are utilities of the tezNames system.

## cTezname

### contract storage

!!! done "storage"

    ```ocaml
    type tTeznameType
        = Regular | Premium | Restricted of bytes
    type tTeznameInfo
        = RegularName
        | PremiumName    of tMerkleProof
        | RestrictedName of tMerkleProof * bytes
    type tMerkleProof = ((bytes, bytes) variant) list
    type tTeznameStorage =
        { sTezname       : tHashedName
        ; sAdmin         : address
        (* -- ownership -- *)
        ; sDest          : address
        ; sOwner         : address
        (* -- advanced info -- *)
        ; sTeznameType    : tTeznameType
        ; sSubnameRecords : tRecords
        (* -- temporal info -- *)
        ; sRegistrationDate  : timestamp
        ; sExpirationDate    : timestamp
        ; sLastModification  : timestamp * string
        }
    ```

### contract entry

!!! done "storage"

    ```ocaml
    resetOwnership : (address * address * tTeznameType) -> _
    updateSubnameRecord : (bytes * address) -> _
    update : ((address, address) variant) -> _
    renew : unit -> _
    ```

## cGate

### contract storage

!!! done "storage"

    ```ocaml
    type tAuctions =
        (bytes, (tez * address * timestamp)) map
    type tCommission =
        { regP : tez
        ; preP : tez
        ; resP : tez
        }
    type tGateStorage =
        { sAdmin      : address
        ; scNameTypes : address
        ; scRootname  : address
        ; sCommission : tCommission
        ; sAuctions   : tAuctions
        }
    ```

### contract entry

!!! done "entries"

    ```ocaml
    register : (bytes * tTeznameInfo) -> _
    resolve : bytes  -> _
    setup : (address * address     * tCommission ) -> _
    ```

## cNameTypes

### contract storage

!!! done "storage"

    ```ocaml
    type tNameTypesStorage =
        { sPremiumRoot    : tMerkleRoot
        ; sRestrictedRoot : tMerkleRoot
        }
    ```

### contract entry

!!! done "entry"

    ```ocaml
    nameCheck : (bytes * tTeznameInfo) -> _
    update : (bytes * bytes) -> _
    ```

<_Details to be written_>
