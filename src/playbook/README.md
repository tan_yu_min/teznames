Deploying the entire teznames system as in demo requires the following steps:

**step 1**

Make sure the following variables are set correctly in file `ENV`.

- `teznamesadmin` - tz-address as system admin
- `user1`, `user2` and `user3` - tz-address for deploying basic teznames

**step 2**

Run the following script to deploy the core contracts, including one _gate_, one _nametype_ and one root _tezname_.

```bash
> ./src/playbook/deploy_core.sh
```

All the address for deployed core contracts will be listed in file `ADDR`.

**step 3**

Run the follwoing script to deploy several _level-1_ teznames based on what you have set for `basenames` in `ENV`. This will use those three `user`s variables in `ENV` as the owner of deployed tezname.

```bash
> ./src/playbook/deploy_basenames.sh
```

All the address for deployed tezname will be appended to file `L1TEZNAMES`.

Additionally, links (to _better-call-dev_) for generated contracts will be listed in [links.md](https://gitlab.com/tezos-southeast-asia/teznames/blob/dev/links.md).
