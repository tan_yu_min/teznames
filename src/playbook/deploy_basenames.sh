#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir=$(git rev-parse --show-toplevel) || exit 1
else
  basedir="$1"
fi
echo -e "\033[36m[info]\033[39m set basedir=${basedir}"
#
echo -e "\033[36m[info]\033[39m loading ${basedir}/ENV.."
source "${basedir}/ENV" || exit 1
#
echo -e "\033[36m[info]\033[39m loading ${basedir}/ADDR.."
source "${basedir}/ADDR"  || exit 1
#
if [ -f "${basedir}/L1TEZNAMES" ]; then
  rm "${basedir}/L1TEZNAMES"
  touch "${basedir}/L1TEZNAMES"
else
  touch "${basedir}/L1TEZNAMES"
fi

#
# ==========================
# ===== deploy tezname =====
# ==========================
#

function getHashedName {
  json='{ "data": { "string": "'
  json+="$1"
  json+='" }, "type": { "prim": "string" }, "gas": "800000" }'
  info=$( curl -s -i \
              -X POST -H 'Content-Type: application/json' \
              -d "${json}" \
              "http://${tezosnode}:8732/chains/main/blocks/head/helpers/scripts/pack_data" \
        | grep -oP "(?<=\"packed\":\").+(?=\",)" \
        || exit 1)
  echo "${info}" # as return
}

function pickRandomUser {
  rand=$(echo $((1 + RANDOM % 3)))
  case "${rand}" in
    1) output="${user1}" ;;
    2) output="${user2}" ;;
    *) output="${user3}" ;;
  esac
  echo "${output}"
}

depLv1Name () {
    tezname=$(echo "$1" | tr '[:upper:]' '[:lower:]')
    echo -e "\033[36m[info]\033[39m hashing ${tezname}"
    pname=$(getHashedName "${tezname}")
    # echo "pname=${pname}"
    hname=$(echo -n "${pname}" | xxd -r -p | openssl dgst -sha256 | awk '{print $2}')
    # echo "hname=${hname}"
    user=$(pickRandomUser)
    # echo "user=${user}"
    # tezos-client -A "${tezosnode}" hash data "\"${tezname}\"" of type string 2>&1 || exit 1
    # hname_short=$(echo "$hname" | cut -c3-10)
    #
    initS=""
    initS+="(Pair 0x${hname} "  # %sTezname
    initS+="(Pair \"${admin}\" " # %sAdmin
    initS+="(Pair \"${user}\" " # %sDest
    initS+="(Pair \"${user}\" " # %sOwner
    initS+="(Pair (Left Unit) " # %sTeznameType
    initS+="(Pair {} "           # %sSubnameRecords
    initS+="(Pair \"2019-10-01T00:00:01Z\" "    # %sRegistrationDate
    initS+="(Pair \"2020-10-01T00:00:01Z\" "    # %sExpirationDate
    initS+="(Pair \"2019-10-01T00:00:01Z\" \"REGISTERED\")" # %sLastModification
    initS+="))))))))"
    #
    flattenversion=$(echo "${version}" | tr -d '.')
    contractName="tezname_${flattenversion}_${tezname}"
    echo -e "\033[36m[info]\033[39m deploying ${contractName}"
    #
    /nix/store/bgqva3wgi3knivdk9pf7gdd0384hj2qf-tezos-0.0.0/bin/tezos-client -A "${tezosnode}"\
      originate contract "${contractName}" \
      transferring 0 from "$admin" \
      running "${contractdir}/cTezname.tz" \
      --init "$initS" \
      --burn-cap 16.0 \
      --force \
      > "${logdir}/${contractName}.deployment" 2>&1 || exit 1
    sleep 1
    #
    nameAddr=$(grep "New contract" "${logdir}/${contractName}.deployment" | awk '{print $3}') || exit 1
    echo "${contractName}=${nameAddr}" >> "${basedir}/L1TEZNAMES"
    echo -e "\033[36m[info]\033[39m \033[33m${tezname}\033[39m has been originated at \033[32m$nameAddr\033[39m"
    #
    /nix/store/bgqva3wgi3knivdk9pf7gdd0384hj2qf-tezos-0.0.0/bin/tezos-client -A "${tezosnode}" \
      transfer 0 from "${teznamesadmin}" \
      to "${ROOTNAMEADDR}" \
      --entrypoint 'entry_updateSubnameRecord' \
      --arg "(Pair 0x${hname} \"${nameAddr}\")" \
      --burn-cap 16.0 \
      > "${logdir}/${contractName}.update" 2>&1 || exit 1
    #
    echo -e "\033[36m[info]\033[39m record on rootname has been updated by adding "
    echo -e "    \033[32m${tezname} ↦ ${nameAddr}\033[39m"
    #
}
#
msg="\033[36m[info]\033[39m lv1 teznames, "
for name in "${basenames[@]}"
do
  depLv1Name "${name}"
  msg+="\033[32m${name}\033[39m "
done
msg+=",were deployed successfully"
echo -e "\033[36m[info]\033[39m basic level1 teznames have been deployed successfully"
#
eval "${scriptdir}/linksgen.sh"
#
